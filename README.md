# README #

This is a TicTacToe Web App implemented using Node.js and Socket.io.

### Author ###

* Donaldson Tan

### How do I get set up? ###

```
$ npm i && npm start
```
And point your browser to `http://localhost:3000`. Optionally, specify
a port by supplying the `PORT` env variable.

### What is this repository for? ###

* To demonstrate the author's proficiency in Javascript and the following stack:
    * Node.js
    * socket.io
    * React.js
    
### Features ###

* Real-time game player between 2 players.
* Limited up to 10 simultaneous game sessions.
* Highlight winning connected line.

### Future Plan ###

* Implementation of Artifical Intelligence for computer to play against a person.
* Live broadcast of current game sessions


### State Machine Model for Server ###
![alt text](tictactoe-server_model.png "State Machine Model for Server")
### State Machine Model for Client ###
![alt text](tictactoe-client_model.png "State Machine Model for Client")