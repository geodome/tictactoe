// Setup HTTP Server
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;


// Define state for State Machine Model 
const STATES = {
    WAIT: "WAIT",
    READY: "READY",
    PLAY: "PLAY",
    COMPLETE: "COMPLETE",
    RESET: "RESET",
    UPDATE: "UPDATE",
    REJECT: "REJECT",
    TEST: "TEST"
};
const GAME = {
    INPROGRESS: 0,
    WIN: 1,
    DRAW: 2
};
const MAXPLAYERS = 2;
const NOROOM = -1;
const MAXPIECES = 9;
const LINES = {
    HORIZONTAL: 0,
    VERTICAL: 1,
    DIAGONAL: 2
};

function createBoard() {
    return [[0,0,0],[0,0,0],[0,0,0]];
}

function updateBoard(sess, row, col) {
    let n = 1;
    let winScore = 3;
    if(sess.turn == 2) {
        n = 5;
        winScore = 15;
    }
    // Add piece to Board
    sess.pieces += 1;
    sess.board[row][col] = sess.role;
    sess.hsum[row] += n;
    sess.vsum[col] += n;
    if(row == col) {
        sess.dsum[0] += n;
    }
    if(col + row == 2) {
        sess.dsum[1] += n;
    }   

    // Check if current player has won
    // Also indicate where the line connecting 3 rooks is.
    let h = sess.hsum.findIndex((s) => { return s == winScore; }) 
    if(h > -1) {
        sess.highlightwin = {line: LINES.HORIZONTAL, index: h};
        return GAME.WIN;
    }
    let v = sess.vsum.findIndex((s) => { return s == winScore; }) 
    if(v > -1) {
        sess.highlightwin = {line: LINES.VERTICAL, index: v};
        return GAME.WIN;
    }
    let d = sess.dsum.findIndex((s) => { return s == winScore; }) 
    if(d > -1) {
        sess.highlightwin = {line: LINES.DIAGONAL, index: d};
        return GAME.WIN;
    }    
    if(sess.pieces == MAXPIECES) {        
        return GAME.DRAW;
    }
    return GAME.INPROGRESS;
    

}

function getRole() {
    return Math.floor((Math.random()*2)+1);
}

// Setup for max 10 simultaneous game sessions
var sessions = [
    {room: "0", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "1", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "2", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "3", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "4", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "5", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "6", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "7", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "8", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
    {room: "9", state: STATES.WAIT, players: 0, role: getRole(), turn: 1, hsum: [0, 0, 0], vsum: [0, 0, 0], dsum: [0, 0], pieces: 0, board: [] },
];

var rooms = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
// Initial session
var availableRoom = rooms.pop();
var whichRoom = {};

// Route "/" to index.html
app.use(express.static(__dirname ));

function msgWaitRoomRole(sess) {
    if(sess.players == 1) {
        return {room: sess.room, role: sess.role};
    } 
    if(sess.players == 2) {
        return {room: sess.room, role: 3 - sess.role};
    } 
}

function emptySession(currentSession) {
    // empty session and make room available
    let rm = parseInt(currentSession.room);
    if(currentSession.state != STATES.WAIT) {
        currentSession.state = STATES.WAIT;
        currentSession.players = 0;
        currentSession.turn = 1;
        currentSession.role = getRole();
        currentSession.hsum = [0, 0, 0];
        currentSession.vsum = [0, 0, 0];
        currentSession.dsum = [0, 0]
        currentSession.pieces = 0;
        currentSession.board = [];        
        rooms.push(rm);
        console.log("EMPTY", rm);
    }
    
}

// When Client is in WAIT state    
io.on('connection', function(socket){   
    socket.on(STATES.WAIT, () => {
        console.log("WAIT ");        
        if (availableRoom == NOROOM) {
            socket.emit(STATES.REJECT); 
            console.log("REJECT", socket.id, "NO ROOM");
        }
        else {                
            let currentSession = sessions[availableRoom];          
            if(currentSession.players < MAXPLAYERS) {
                // add socket to current room and assign player role
                currentSession.players += 1;
                socket.join([currentSession.room]);
                whichRoom[socket.id] = currentSession.room;
                socket.emit(currentSession.state, msgWaitRoomRole(currentSession));
                console.log("JOIN", currentSession.room, socket.id);
                // Client responds by shifting to READY state
            } 
            if (currentSession.players == MAXPLAYERS) {
                // shift state from WAIT to READY
                currentSession.state = STATES.READY;
                currentSession.board = createBoard();
                socket.to(currentSession.room).emit(currentSession.state);
                socket.emit(currentSession.state);
                console.log("READY", currentSession.room);
                // if Client is Player 1, Client responds by shifting to PLAY state.
                // if Client is Player 2, Client responds by shifting to UPDATE state
                // shift session state from READY to PLAY
                currentSession.state = STATES.PLAY;                
                socket.to(currentSession.room).emit(currentSession.state);
                console.log("PLAY", currentSession.room);                
                availableRoom = rooms.pop() || NOROOM;
                console.log("AVAILABLE ROOM", availableRoom);
            }
        } 
    });
});

// When Client is in READY state, it doesn't transmit any messages, so no need to handle READY events from Client.
 
// When Client is in PLAY STATE
io.on('connection', function(socket) {
    socket.on(STATES.PLAY, (msg) => {        
        let rm = whichRoom[socket.id];
        let currentSession = sessions[rm];
        console.log("ROOK", rm, msg)         
        if( currentSession.turn == msg.role) {
            
            // Update board with new position
            if (currentSession.board[msg.row][msg.col] == 0) {                
                let outcome = updateBoard(currentSession, msg.row, msg.col);
                switch(outcome) {
                    case GAME.INPROGRESS:
                        currentSession.turn = 3 - currentSession.turn;
                        msg.outcome = GAME.INPROGRESS;
                        currentSession.state = STATES.UPDATE;                        
                        socket.to(currentSession.room).emit(currentSession.state, msg);
                        //socket.emit(currentSession.state, msg);
                        currentSession.state = STATES.PLAY;                        
                        break;
                    case GAME.WIN:                        
                        msg.outcome = GAME.WIN;
                        msg.winner = msg.role;
                        msg.highlightwin = currentSession.highlightwin;
                        currentSession.state = STATES.UPDATE;                                                
                        socket.to(currentSession.room).emit(currentSession.state, msg);
                        socket.emit(currentSession.state, msg);                        
                        currentSession.state = STATES.COMPLETE;
                        socket.to(currentSession.room).emit(currentSession.state, msg);
                        socket.emit(currentSession.state, msg);                        
                        // Client responds by shifting to COMPLETE state                       
                        break;
                    case GAME.DRAW:
                        msg.outcome = GAME.DRAW;
                        currentSession.state = STATES.UPDATE;                                                
                        socket.to(currentSession.room).emit(currentSession.state, msg);
                        socket.emit(currentSession.state, msg);                                                                 
                        currentSession.state = STATES.COMPLETE;
                        socket.to(currentSession.room).emit(currentSession.state);
                        socket.emit(currentSession.state);
                        // Client responds by shifting to COMPLETE state
                        break;
                }
                // socket.to(currentSession.room).emit(STATES.UPDATE, msg);                
                console.log("OUTCOME", currentSession.room, outcome, msg);
            }
        }
        
    });
});

// When Client is in UPDATE state, it doesn't transmit any messages, so no need to handle UPDATE events from Client.
    

// When Client is in COMPLETE state
io.on('connection', function(socket) {
    socket.on(STATES.COMPLETE, (msg) => {
        let rm = whichRoom[socket.id];
        let currentSession = sessions[rm];
        emptySession(currentSession);
        delete whichRoom[socket.id];
        socket.disconnect();
        console.log("COMPLETE", rm);
    }); 
});

// When Client disconnects unexpectedly
io.on('connection', function(socket) {
    socket.on("disconnect", (msg) => {        
        let rm = whichRoom[socket.id];
        if(rm) {
            let currentSession = sessions[rm];
            currentSession.state = STATES.COMPLETE;
            socket.to(currentSession.room).emit(STATES.RESET);
            emptySession(currentSession);
            delete whichRoom[socket.id];
            console.log("DISCONNECT", rm, socket.id);
        }
        
    });
});

// Begin listening
http.listen(port, function(){
    console.log('listening on *:' + port);
    console.log("AVAILABLE ROOM", availableRoom);
});
